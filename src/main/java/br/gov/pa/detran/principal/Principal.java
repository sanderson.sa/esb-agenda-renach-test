package br.gov.pa.detran.principal;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.commons.io.FileUtils;

import br.gov.pa.detran.ConsultaAgendaExameMonitoramentoEnvDTO;
import br.gov.pa.detran.ConsultaAgendaExameMonitoramentoRetDTO;
import br.gov.pa.detran.ConsultaAgendaExameMonitoramentoRetDTOArray;
import br.gov.pa.detran.ConsultaFotoEnvDTO;
import br.gov.pa.detran.ConsultaFotoRetDTO;
import br.gov.pa.detran.ExameFaltaEnvDTO;
import br.gov.pa.detran.ExameProfissionalMonitoramentoEnvDTO;
import br.gov.pa.detran.RegistraResultadoExameMonitoramentoEnvDTO;
import br.gov.pa.detran.RegistraResultadoExameMonitoramentoRetDTO;
import br.gov.pa.detran.ValidaColetaDigitalMonitoramentoEnvDTO;
import br.gov.pa.detran.ValidaColetaDigitalMonitoramentoRetDTO;
import br.gov.pa.detran.authentication.AcessoExternoEnvDTO;
import br.gov.pa.detran.authentication.AuthenticationWSResource;
import br.gov.pa.detran.authentication.AuthenticationWSResourceService;
import br.gov.pa.detran.authentication.ValidaUsuarioExternoEnvDTO;
import br.gov.pa.detran.authentication.ValidaUsuarioExternoRetDTO;
import br.gov.pa.detran.esb.MonitoramentoRenachWSResource;
import br.gov.pa.detran.esb.MonitoramentoRenachWSResourceService;


public class Principal {

	final static String LOGIN = "clevan.costa";
	final static String SENHA = "detran"; 
	
	public static void main(String[] args) {
		//Passa usuário a ser validado
		ValidaUsuarioExternoEnvDTO autenticacao = new  ValidaUsuarioExternoEnvDTO();
		autenticacao.setLogin(LOGIN);
		autenticacao.setSenha(SENHA);
		
		ValidaUsuarioExternoRetDTO validaUsuarioExternoRetDTOResp = null;
		
		try {
			//Pegar passaporte
			AuthenticationWSResource serviceAuthentication = new AuthenticationWSResourceService().getAuthenticationWSResourcePort();					
			validaUsuarioExternoRetDTOResp = serviceAuthentication.validaUsuarioExterno(autenticacao);
			
			//Criar dto com usuário autorizado
			AcessoExternoEnvDTO acesso = new AcessoExternoEnvDTO();
			acesso.setLogin(LOGIN);
			acesso.setPassaporte(validaUsuarioExternoRetDTOResp.getPassaporte());
			
			//Instanciar servicos da interface
			MonitoramentoRenachWSResource detranAgendaEsbService = new MonitoramentoRenachWSResourceService().getMonitoramentoRenachWSResourcePort();
			
			//Validar métodos .. seguir ordem correta
			
			//registraResultadoExameMonitoramento
			registraResultadoExameMonitoramento(acesso, detranAgendaEsbService);
			
			//consultaAgendaExameMonitoramento
			consultaAgendamentoExameMonitoramento(acesso, detranAgendaEsbService);
			
			//consultaFoto
			consultaFoto(acesso, detranAgendaEsbService);
			
			//validaColetaDigitalMonitoramento - chamado dentro do consulta foto
			validaColetaDigitalMonitoramento(acesso, detranAgendaEsbService);

			//
		} catch (com.sun.xml.internal.ws.fault.ServerSOAPFaultException e) {
			System.out.println("Usuário não localizado");
		} catch (Exception e) {
			System.out.println(validaUsuarioExternoRetDTOResp.getPassaporte());
		}
		
	}
	
	
	
	private static void validaColetaDigitalMonitoramento(AcessoExternoEnvDTO acesso, MonitoramentoRenachWSResource detranAgendaEsbService) {
		System.out.println(" ## VALIDA COLETA DIGITAL MONITORAMENTO ...");
		
		ValidaColetaDigitalMonitoramentoEnvDTO env = new ValidaColetaDigitalMonitoramentoEnvDTO();
		env.setLogin(acesso.getLogin());
		env.setCdUsuarioLogado(0);
		env.setPassaporte(acesso.getPassaporte());
		env.setNmMaquina("teste");
		env.setCpf("44255012253");
		env.setDedoColetado(2);
		env.setNoFormularioRenach(0L);
		env.setTipoPessoa(5);	
		env.setTipoImagem(1);
		try {
			env.setImagemDigitalCapturada(new Principal().readImage());
			ValidaColetaDigitalMonitoramentoRetDTO resp = detranAgendaEsbService.validaColetaDigitalMonitoramento(acesso, env);
			if(resp != null) {
				System.out.println(" ## RETORNO: " + resp.getCodigoRetorno() + " - " + resp.getMensagem());
			}else {
				System.out.println(" ## ERRO AO VALIDA COLETA ...");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void registraResultadoExameMonitoramento(AcessoExternoEnvDTO acesso, MonitoramentoRenachWSResource detranAgendaEsbService) {
		System.out.println(" ## REGISTRAR RESULTADO EXAME MONITORAMENTO...");
		RegistraResultadoExameMonitoramentoEnvDTO env = new RegistraResultadoExameMonitoramentoEnvDTO();
		List<ExameFaltaEnvDTO> arrayExameFaltaEnvDTO = new ArrayList<ExameFaltaEnvDTO>();
		ExameFaltaEnvDTO falta = new ExameFaltaEnvDTO();
		falta.setIdFalta(52L);
		falta.setPontosNegativos(2);
		env.getArrayExameFaltaEnvDTO().addAll(arrayExameFaltaEnvDTO);
		
		List<ExameProfissionalMonitoramentoEnvDTO> arrayExameProfissionalEnvDTO = new ArrayList<ExameProfissionalMonitoramentoEnvDTO>();
		ExameProfissionalMonitoramentoEnvDTO prof1 = new ExameProfissionalMonitoramentoEnvDTO();
		prof1.setCpf("44255012253");
		prof1.setObservacao("");
		arrayExameProfissionalEnvDTO.add(prof1);
		ExameProfissionalMonitoramentoEnvDTO prof2 = new ExameProfissionalMonitoramentoEnvDTO();
		prof1.setCpf("76534871220");
		prof1.setObservacao("");
		arrayExameProfissionalEnvDTO.add(prof2);
		env.getArrayExameProfissionalEnvDTO().addAll(arrayExameProfissionalEnvDTO);
		
		env.setCdResultado(1L);
		env.setCdUsuarioLogado(0);
		env.setCpf("75165848272");
		env.setDtExame("05/11/2018");
		env.setIdCLF(889L);
		env.setLogin(acesso.getLogin());
		env.setPassaporte(acesso.getPassaporte());
		env.setNmMaquina("m");
		env.setNoFormularioRenach(300011213L);
		try {
			RegistraResultadoExameMonitoramentoRetDTO resp = detranAgendaEsbService.registraResultadoExameMonitoramento(acesso, env);
			if(resp != null) {
				if(resp.getCodigoRetorno() == 1) {
					System.out.println(" ## REGISTRAR RESULTADO EXAME MONITORAMENTO... REALIZADO");
				}else {
					System.out.println(" ## ERRO AO REGISTRAR EXAME: " + "COD.: " + resp.getCodigoRetorno() + " MSG: " + resp.getMensagem());
				}
				
			}
			
		} catch (Exception e) {
			System.out.println(" ## ERRO AO REGISTRAR RESULTADO EXAME MONITORAMENTO...");
		}
		
	}
	
	private static void consultaFoto(AcessoExternoEnvDTO acesso, MonitoramentoRenachWSResource detranAgendaEsbService) {
		System.out.println(" ## CONSULTA FOTO...");
		ConsultaFotoEnvDTO consultaFotoEnvDTO = new ConsultaFotoEnvDTO();
		consultaFotoEnvDTO.setLogin(acesso.getLogin());
		consultaFotoEnvDTO.setCdUsuarioLogado(0);
		consultaFotoEnvDTO.setPassaporte(acesso.getPassaporte());
		consultaFotoEnvDTO.setNmMaquina("teste");
		consultaFotoEnvDTO.setCpfColetado("66484189234");
		consultaFotoEnvDTO.setNoFormularioRenach(100645992L);
		consultaFotoEnvDTO.setTipoPessoa(1);
		try {
			ConsultaFotoRetDTO consultaFoto = detranAgendaEsbService.consultaFoto(acesso, consultaFotoEnvDTO);
			System.out.println(" ## FOTO:" + consultaFoto.getFotoBase64());
			salvarImagem(consultaFoto.getFotoBase64());
		}catch (Exception e) {
			System.out.println(" ## ERRO AO CONSULTA FOTO. " + e.getMessage());
		}
		
	}
	

	private static void consultaAgendamentoExameMonitoramento(AcessoExternoEnvDTO acesso, MonitoramentoRenachWSResource detranAgendaEsbService) {
		System.out.println(" ## CONSULTAR EXAME MONITORAMENTO...");		
		ConsultaAgendaExameMonitoramentoEnvDTO env = new ConsultaAgendaExameMonitoramentoEnvDTO();
		env.setIdCLF(889L);
		env.setCdUsuarioLogado(0);
		env.setLogin(acesso.getLogin());
		env.setDataAgendaExame("06/11/2018");
		env.setNmMaquina("teste");
		env.setPassaporte(acesso.getPassaporte());							
		try {
			ConsultaAgendaExameMonitoramentoRetDTOArray resp = detranAgendaEsbService.consultaAgendaExameMonitoramento(acesso, env);
			if(resp == null || resp.getItem().isEmpty()) {
				System.out.println(" ## NAÕ EXISTE EXAME ...");
			}else {
				System.out.println(" ## EXAMES ...");
				for (ConsultaAgendaExameMonitoramentoRetDTO i : resp.getItem()) {
					System.out.println("  --> " + i.getExame());
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void salvarImagem(String base64) {
		System.out.println("Salvar imagem");		
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(base64);
			FileUtils.writeByteArrayToFile(new File("imagem.png"), decodedBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String readImage() throws URISyntaxException, IOException {
		// Ler imagem do arquivo
		String content = new String(Files.readAllBytes(Paths.get("imagem.txt")));
		return content;
	}
}
